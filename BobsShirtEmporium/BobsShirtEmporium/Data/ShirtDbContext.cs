﻿using BobsShirtEmporium.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BobsShirtEmporium.Data
{
    public class ShirtDbContext : DbContext
    {
        public ShirtDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Shirt> Shirts { get; set; }
    }
}
