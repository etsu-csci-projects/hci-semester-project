﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BobsShirtEmporium.Models
{
    public class ShirtsSearchViewModel
    {
        public List<Shirt> Shirts { get; set; }
        public SelectList Styles { get; set; }
        public SelectList Colors { get; set; }
        public string ShirtStyle { get; set; }
        public string Color { get; set; }
        public string SearchString { get; set; }
    }
}
