﻿using BobsShirtEmporium.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BobsShirtEmporium.Models
{
    public class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new ShirtDbContext(
                serviceProvider.GetRequiredService<
                    DbContextOptions<ShirtDbContext>>()))
            {
                // Look for any movies.
                if (context.Shirts.Any())
                {
                    return;   // DB has been seeded
                }

                context.Shirts.AddRange(
                    new Shirt 
                    {
                        Title = "Carp Festival Shirt",
                        Description = "Vintage blue shirt with pictures of carp on it",
                        Size = "L",
                        Color = "Blue",
                        Style = "Vintage",
                        Price = 25,
                        Stock = 1,
                        ImagePath = "carpsquare.jpg"
                    },

                    new Shirt 
                    {
                        Title = "Lithuania Dead Tee",
                        Description = "Tie-dye t-shirt made by the Grateful Dead for Lithuania's 1992 Olympic basketball team",
                        Size = "L",
                        Color = "Tie-dye",
                        Style = "Vintage",
                        Price = 500,
                        Stock = 1,
                        ImagePath = "lithsquare.jpg"
                    },

                    new Shirt
                    {
                        Title = "King Gizzard Tee",
                        Description = "Blue King Gizzard and the Lizard Wizard band tee",
                        Size = "L",
                        Color = "Blue",
                        Style = "New",
                        Price = 24.99,
                        Stock = 100,
                        ImagePath = "gizsquare.jpg"
                    },

                    new Shirt
                    {
                        Title = "Toronto Blue Jays Tee",
                        Description = "T-shirt with the team's logo on the front",
                        Size = "XL",
                        Color = "Blue",
                        Style = "New",
                        Price = 19.99,
                        Stock = 20,
                        ImagePath = "jaysquare.jpg"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
